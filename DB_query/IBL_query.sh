#!/bin/bash
#################################################################################
#																																								#
#	To run the scripts 'source IBL_query [-p] start_date stop_date week_string 		#
#																																								#
#################################################################################
usage="$0 [-p] start_date stop_date week_string  : please specify -p for let the  plots show up"

if [ $# -lt 3 ]; then
  echo "Usage: $usage"
  #echo "Options: -k kmervalue (def: $KVALUE) | -c cov_cutoff (def: $CUTOFF)"
  exit 1
fi


start=$1 #03-06-2015
stop=$2 #15-06-2015
week=$3 #W12

mode=aliastext
#echo $4
while getopts ':p:' opt; do
	case "$opt" in
	p) 	mode=aliasplot
			echo $mode
			echo $1 $2 $3
			start=$2 #03-06-2015
			stop=$3 #15-06-2015
			week=$4 #W12
			#exit
			;;

	\?) 
	esac
done
shift $((OPTIND - 1))

#if [ $4 == plot ]
#	then
#	mode=aliasplot
#fi

./dcsDBaccess_lightout.pl $mode 00 00 LI_S%TModule $start 00:00 $stop 09:00 datapoint gnuplot
grep -v 'LI_S15' query_result.txt > query_mod.txt
mv query_mod.txt '../macros/IBL_ThermalSurvey/'$week'_mod_temperature.txt'
./dcsDBaccess_lightout.pl $mode 00 00 LI_S%_M%Power $start 00:00 $stop 09:00 datapoint gnuplot
grep -v 'LI_S15' query_result.txt > query_pow.txt
mv query_pow.txt '../macros/IBL_ThermalSurvey/'$week'_power.txt'
./dcsDBaccess_lightout.pl $mode 00 00 %LI_%ENV_TT%28 $start 00:00 $stop 09:00 datapoint gnuplot
mv query_result.txt query_cool.txt
grep -v 'Elements' query_cool.txt > query_cool_28.txt
grep -v 'Number' query_cool_28.txt > query_cool.txt
./dcsDBaccess_lightout.pl $mode 00 00 %LI_%ENV_TT%30 $start 00:00 $stop 09:00 datapoint gnuplot
cat query_result.txt >> query_cool.txt
mv query_cool.txt '../macros/IBL_ThermalSurvey/'$week'_cool.txt'

# QUERY FOR HV REQUIRES HIGHER BANDWIDTH
for stave in {1..14}
do
	if [ $stave -lt 10 ]
		then
			./dcsDBaccess_lightout.pl $mode 00 00 %LI_S0$stave%_HV_VMeas $start 00:00 $stop 09:00 datapoint gnuplot
		else
			./dcsDBaccess_lightout.pl $mode 00 00 %LI_S$stave%_HV_VMeas $start 00:00 $stop 09:00 datapoint gnuplot
	fi
	if [ $stave -eq 1 ]
		then
			egrep -v 'LI_S15|Data|Numb' query_result.txt > query_HV_V.txt
		else
			echo "APPENDING " $stave
			egrep -v 'LI_S15|Data|Numb' query_result.txt >> query_HV_V.txt
	fi
done
mv query_HV_V.txt '../macros/IBL_ThermalSurvey/'$week'_hv_V.txt'

./dcsDBaccess_lightout.pl $mode 00 00 %LI_S%_HV_IMeas $start 00:00 $stop 09:00 datapoint gnuplot
grep -v 'LI_S15' query_result.txt > query_HV_I.txt
mv query_HV_I.txt '../macros/IBL_ThermalSurvey/'$week'_hv_I.txt'
rm *.dat
