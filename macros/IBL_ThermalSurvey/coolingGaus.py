from ROOT import gROOT, TCanvas, TH1F, TF1
import numpy as np
#import matplotlib.pyplot as plt

MAX_HOUR = 8

gROOT.Reset()
c1 = TCanvas('c1','c1',1400,400)
hist = TH1F('hist','hist', 50, -16.5, -14.5);
file = open('M9_ext_cool.txt','r')
line = file.readline()
temp_list = []
hist_list = []

for i in range(1,15):
	hist = TH1F('stave_'+str(i),'stave_'+str(i), 50, -16.5, -14.5)
	hist_list.append(hist)
while line:
	values = line.split('  ')
	#print line
	if(len(values) == 3):
		NTC_name = values[0]
		NTC_temperature = float(values[1])
		NTC_date = values[2]
		NtcHour =( NTC_date.split(' ') )[1].split(':')
		for i in range(1,15):
			if i<10:
				stave_str = 'LI_S0' + str(i)
			else: 
				stave_str = 'LI_S' +str(i)
			if(int(NtcHour[0])<MAX_HOUR and stave_str in NTC_name):
				print NTC_name + ' ' + NtcHour[0] + ' ' + str(NTC_temperature) 
				temp_list.append([NTC_temperature,i])
				hist_list[i-1].Fill(NTC_temperature)		
	line = file.readline()
file.close()
c1.Divide(7,2)
#hist.SetFillColor('kRed');

overall_histo = TH1F('overall_histo', 'overall_histo', 100, -0.5, 0.5)
#rms_list = []
for i in range(1,15):
	c1.cd(i)
	hist_list[i-1].Draw()
	#hist_list[i-1].Fit('gaus')
	
#	print gaus.GetParameter(2)
for temp in temp_list:
	t = temp[0]
	s = temp[1]
	overall_histo.Fill(t - hist_list[s-1].GetMean())
c2 = TCanvas('c2','c2',800,600)
overall_histo.Draw();
overall_histo.Fit('gaus')
overall_histo.GetFunction('gaus').SetNpx(10000)
print 'SIGMA IS: '+str(overall_histo.GetFunction('gaus').GetParameter(2))
c1.Print('canvas.eps')
c1.WaitPrimitive()
