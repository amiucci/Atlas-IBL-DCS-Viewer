#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "TGraph.h"
#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include <string>
#include <sstream>
#include <fstream>
#include "TMultiGraph.h"
#include "TPad.h"
#include "TLegend.h"
#include "THStack.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TColor.h"
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "TGraphErrors.h"
#include "TPad.h"
#include "TLatex.h"

struct module_data{
	std::string module_temperature;
	std::vector<double> temps;
	std::vector<double> temp_time;
	TH1D* temp_distro;
	TGraph* temp_history;
};

void sort_module_data(std::stringstream& buffer, module_data* MD);
void filter(double delta_time, double delta_temp, std::stringstream& buffer, std::stringstream& selection);
void fill_temp_map(TH2D* map, double temp, std::string module);
void draw_option_temp_map(TH2D* temp_map);


void smoothing( double delta_time, double delta_temp, std::string filename){

	TH2D* mean_map = new TH2D("Mean Temperature Map", "Mean Temperature Map", 8, 0.5, 8.5, 14, 0.5, 14.5);
	TH2D* rms_map = new TH2D("RMS Temperature Map", "RMS Temperature Map", 8, 0.5, 8.5, 14, 0.5, 14.5);
		
	ifstream file;
	file.open(filename.c_str());
	std::stringstream rootfilestream;
	rootfilestream << "smoothing-" << delta_temp<<".root";
	std::string rootfilename = rootfilestream.str();
	TFile *rootfile = new TFile(rootfilename.c_str(),"RECREATE");

	if(file.is_open()){
		std::stringstream filestream, selstream;
		filestream << file.rdbuf();
		filter(delta_time, delta_temp, filestream, selstream);

//		std::cout<<selstream.str();

//		double temp;
//		std::string timestamp, date, hours, module;
//		std::vector<double> vTemp, vTime, vsTemp, vsTime;

//		module_data MD[112];
		module_data MD_sel[112];
		sort_module_data(selstream,MD_sel);
		TCanvas *c1 = new TCanvas("c1","c1",1200,600);
		c1->Divide(2,1);
		for(int i=0; i<112; i++){
			//std::cout<<MD_sel[i].temp_distro->GetEntries()<<std::endl;
			if(i==0){
				c1->cd(1);
				MD_sel[i].temp_distro->Draw();
				c1->cd(2);
				MD_sel[i].temp_history->Draw("APL");	
			}/*else{
				c1->cd(1);
				MD_sel[i].temp_distro->Draw("same");
			}*/
			MD_sel[i].temp_distro->Write();
			MD_sel[i].temp_history->Write();
			fill_temp_map(mean_map, MD_sel[i].temp_distro->GetMean(), MD_sel[i].module_temperature);
			fill_temp_map(rms_map, MD_sel[i].temp_distro->GetRMS(), MD_sel[i].module_temperature);
				
		}
	mean_map->Draw();
	rms_map->Draw();
	draw_option_temp_map(mean_map);
	draw_option_temp_map(rms_map);
	mean_map->Write();
	rms_map->Write();
	}
}

void draw_option_temp_map(TH2D* temp_map){
	temp_map->Draw();

	std::string labels[8] = {"M4C","M3C","M2C","M1C","M1A","M2A","M3A","M4A"};
	for(int i=1; i<=8; i++)temp_map->GetXaxis()->SetBinLabel(i,labels[i-1].c_str());
	temp_map->GetXaxis()->SetTitle("Sector");
	temp_map->GetYaxis()->SetTitle("Stave");
	temp_map->GetXaxis()->SetLabelSize(0.060);
	temp_map->GetYaxis()->SetLabelSize(0.060);
	temp_map->GetXaxis()->SetTitleSize(0.060);
	temp_map->GetYaxis()->SetTitleSize(0.060);
	temp_map->GetXaxis()->SetTitleOffset(0.8);

}
void fill_temp_map(TH2D* map, double temp, std::string module){
	int stave_id = atoi(module.substr(4,2).c_str());
	//std::cout<<"MODULE "<<module<<std::endl;
//	if(module.find("_C") != std::string::npos && !module_check) map->SetBinContent(1,stave_id, temp);	
	if(module.find("C_M4") != std::string::npos) map->SetBinContent(1,stave_id,temp);
	if(module.find("C_M3") != std::string::npos) map->SetBinContent(2,stave_id,temp);
	if(module.find("C_M2") != std::string::npos) map->SetBinContent(3,stave_id,temp);
	if(module.find("C_M1") != std::string::npos) map->SetBinContent(4,stave_id,temp);
	if(module.find("A_M1") != std::string::npos) map->SetBinContent(5,stave_id,temp);
	if(module.find("A_M2") != std::string::npos) map->SetBinContent(6,stave_id,temp);
	if(module.find("A_M3") != std::string::npos) map->SetBinContent(7,stave_id,temp);
	if(module.find("A_M4") != std::string::npos) map->SetBinContent(8,stave_id,temp);
//	if(module.find("_A") != std::string::npos && !module_check) map->SetBinContent(10,stave_id, temp);	
}

void filter(double delta_time, double delta_temp, std::stringstream& buffer, std::stringstream& selection){

		std::string timestamp,module, date, hours, check_module;
		double temp,check_time,check_temp;
		int counter=0;
		while(buffer){
			buffer >> module;
			if(module.find("LI_S") != std::string::npos)
			{
				buffer >> temp;
				buffer >> date;
				buffer >> hours;

				timestamp = date + "_" + hours.substr(0,8);

				struct tm tm;
				time_t epoch;
	
				
				if ( strptime(timestamp.c_str(), "%d-%m-%Y_%H:%M:%S", &tm) != NULL ){
					//epoch = mktime(&tm);
					epoch = timegm(&tm);

					if(counter == 0 || (epoch - check_time) > delta_time || abs(temp - check_temp) > delta_temp){
						selection << module << "\t" <<  temp << "\t" <<  epoch <<std::endl;
						check_temp = temp;
						check_time = epoch;
						check_module = module;
						//std::cout<<"DELTA"<< (double)( epoch - check_time) << "\tCOUNTER " << counter <<std::endl;
					}
					counter++;
					if(check_module != module) counter=0;
				}
			}
		}
}

void sort_module_data(std::stringstream& buffer, module_data* MD){
	int buffer_counter = 0;
	std::string module;
	double time;
	double temperature;

	int stave_id, stave_side_offset, module_id, m_index=0;
	buffer.seekg(0,ios::beg);

	while(!buffer.eof()){
		buffer_counter++;
		buffer>>module;	
		stave_id = atoi(module.substr(4,2).c_str());
		if(module.substr(7,1)=="A")stave_side_offset=0;
		else stave_side_offset=4;	
		module_id = atoi(module.substr(10,1).c_str());
		m_index = (stave_id-1)*8+stave_side_offset+(module_id-1);
		buffer>>temperature;
		buffer>>time;
		if(MD[m_index].temps.size()==0){
			MD[m_index].temp_distro = new TH1D(module.c_str(), module.c_str(), 1000, -30, 30);

			if(m_index!=0){
				MD[m_index-1].temp_history = new TGraph(MD[m_index-1].temps.size(),&(MD[m_index-1].temp_time[0]),&(MD[m_index-1].temps[0]));
				MD[m_index-1].temp_history->SetTitle(MD[m_index-1].module_temperature.c_str());
				MD[m_index-1].temp_history->SetName(MD[m_index-1].module_temperature.c_str());
			}
		}
		MD[m_index].temps.push_back(temperature);
		MD[m_index].temp_time.push_back(time);
		//temps.push_back(temperature);
		//temp_time.push_back(time);
		MD[m_index].module_temperature = module;
		MD[m_index].temp_distro->Fill(temperature);
		//std::cout<<MD[m_index].module_temperature<<"\t"<<temperature<<"\t"<<time<<"\t"<<m_index<<std::endl;	
	}

	MD[m_index].temp_history = new TGraph(MD[m_index].temps.size(),&(MD[m_index].temp_time[0]),&(MD[m_index].temps[0]));
	MD[m_index].temp_history->SetTitle(MD[m_index].module_temperature.c_str());
	MD[m_index].temp_history->SetName(MD[m_index].module_temperature.c_str());


//	MD[m_index].temp_distro->Draw();
}
