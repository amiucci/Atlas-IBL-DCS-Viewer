#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "TGraph.h"
#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include <string>
#include <sstream>
#include <fstream>
#include "TMultiGraph.h"
#include "TPad.h"
#include "TLegend.h"
#include "THStack.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TColor.h"
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TPad.h"
#include "TLatex.h"

struct run_time_interval{
	std::string run_number;
	double start_time;
	double stop_time;
	double mean_temperature;
	double mean_rms;
	double mean_temperature_cool;
	double mean_rms_cool;
};

void drawAtlasInternal1D(float xmin = 0.75, float ymax = 0.80, float margin = 0.055);

void PlotRuns(std::string week){
std::string setpoint_file = week + "_set_point.txt";
if(week == "W1") setpoint_file = "M9_set_point.txt";
std::string runlist_file = week + "_runQuery.txt";
std::string temp_file = week + "_mod_temperature.txt";
std::string folder = "/afs/cern.ch/user/a/amiucci/work/public/IBL_temp_monitoring/"+runlist_file.substr(0,runlist_file.size()-4)+"/";

	std::vector<run_time_interval> RTIs;

	std::string run;
	double start_t, stop_t;
	ifstream runListFile;
	runListFile.open(runlist_file.c_str());
	std::stringstream buffer;
	buffer << runListFile.rdbuf();
	while(buffer){
		buffer>>run;
		buffer>>start_t;
		buffer>>stop_t;
		run_time_interval RTI;
		RTI.run_number = run;
		RTI.start_time = start_t;
		RTI.stop_time = stop_t;
		RTIs.push_back(RTI);
		std::cout<<run<<std::endl;
	} 
	runListFile.close();



//std::string run_list[4] = {"255313.root","255788.root", "255988.root", "256147.root"};
//double setPoint_list[4] = {15.,7.,0.,-10.};
//std::vector<TFile*> file_list;
//double mean_temp[4], mean_rms[4], delta_temp[4], rms_rms[4];
THStack* module_p2p_stack = new THStack("module_p2p_stack","module_p2p_stack;Peak to Peak #Delta T ([#circC]);");
THStack*cool_p2p_stack = new THStack("cool_p2p_stack","cool_p2p_stack;Peak to Peak #Delta T ([#circC]);");

TFile* file;
	std::vector<double> mean_temps, mean_rms, mean_p2p, times, time_widths, err_temps, err_rms, err_p2p_max, err_p2p_min;
	std::vector<double> mean_temps_cool, mean_rms_cool,mean_p2p_cool, err_temps_cool, err_rms_cool, err_p2p_cool_max, err_p2p_cool_min; 
	for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
		std::string filename = folder + (*it).run_number + ".root";
		file = new TFile(filename.c_str());
		TH1D* temp_mean_distro = (TH1D*)(file->Get("temp_mean_distro"));
		TH1D* temp_p2p_list = (TH1D*)(file->Get("temp_PeakToPeak_list"));
		TH1D* temp_p2p_distro = (TH1D*)(file->Get("temp_p2p_distro"));
		module_p2p_stack->Add(temp_p2p_distro);
		mean_temps.push_back(temp_mean_distro->GetMean());
		err_temps.push_back(temp_mean_distro->GetRMS());
		TH1D* cool_mean_distro = (TH1D*)(file->Get("cool_mean_distro_outlet"));
		mean_temps_cool.push_back(cool_mean_distro->GetMean());
		err_temps_cool.push_back(cool_mean_distro->GetRMS());
		mean_p2p.push_back(temp_p2p_distro->GetMean());
		double tmax = (double)temp_p2p_list->GetMaximum();
		double tmin = (double)temp_p2p_list->GetMinimum();
		std::cout<<"RUN:"<< (*it).run_number<<"\tMaximum: "<<tmax<<"\tMinimum: "<<tmin<<std::endl;
		err_p2p_max.push_back(tmax-temp_p2p_distro->GetMean());
		err_p2p_min.push_back(temp_p2p_distro->GetMean()-tmin);
		(*it).mean_temperature = temp_mean_distro->GetMean();
		(*it).mean_temperature_cool = cool_mean_distro->GetMean();

		TH1D* temp_rms_distro = (TH1D*)(file->Get("temp_rms_distro"));
		mean_rms.push_back(temp_rms_distro->GetMean());
		err_rms.push_back(temp_rms_distro->GetRMS());
		TH1D* cool_rms_distro = (TH1D*)(file->Get("cool_rms_distro_outlet"));
		TH1D* cool_p2p_list = (TH1D*)(file->Get("cool_p2p_list_outlet"));
		TH1D* cool_p2p_distro = (TH1D*)(file->Get("cool_p2p_distro_outlet"));
		cool_p2p_stack->Add(cool_p2p_distro);
		mean_rms_cool.push_back(cool_rms_distro->GetMean());
		err_rms_cool.push_back(cool_rms_distro->GetRMS());
		mean_p2p_cool.push_back(cool_p2p_distro->GetMean());
		double max = (double)cool_p2p_list->GetMaximum();
		double min = (double)cool_p2p_list->GetMinimum();
		//std::cout<<"RUN:"<< (*it).run_number<<"\tMaximum: "<<max<<" | bMax:"<<cool_p2p_list->GetMaximum()<<"\tMinimum: "<<min<<std::endl;
		err_p2p_cool_max.push_back(max-cool_p2p_distro->GetMean());
		err_p2p_cool_min.push_back(cool_p2p_distro->GetMean()-min);
		//delta_temp[i] = mean_temp[i]-setPoint_list[i];
		(*it).mean_rms = temp_rms_distro->GetMean();
		(*it).mean_rms_cool = cool_rms_distro->GetMean();		

		times.push_back(((*it).stop_time + (*it).start_time)/2);
		time_widths.push_back(((*it).stop_time - (*it).start_time)/2);
//		file->Close();
	}

TGraphErrors *mTemp = new TGraphErrors(mean_temps.size()-1,&(times[0]),&(mean_temps[0]),&(time_widths[0]),&(err_temps[0]));
TGraphErrors *rTemp = new TGraphErrors(mean_rms.size()-1,&(times[0]),&(mean_rms[0]),&(time_widths[0]),&(err_rms[0]));
TGraphAsymmErrors *pTemp = new TGraphAsymmErrors(mean_p2p.size()-1,&(times[0]),&(mean_p2p[0]),&(time_widths[0]),&(time_widths[0]),&(err_p2p_min[0]),&(err_p2p_max[0]));
mTemp->SetTitle("Average temperatures");
rTemp->SetTitle("Average RMS");

TGraphErrors *mTemp_cool = new TGraphErrors(mean_temps_cool.size()-1,&(times[0]),&(mean_temps_cool[0]),&(time_widths[0]),&(err_temps_cool[0]));
TGraphErrors *rTemp_cool = new TGraphErrors(mean_rms_cool.size()-1,&(times[0]),&(mean_rms_cool[0]),&(time_widths[0]),&(err_rms_cool[0]));
TGraphAsymmErrors *pTemp_cool = new TGraphAsymmErrors(mean_p2p_cool.size()-1,&(times[0]),&(mean_p2p_cool[0]),&(time_widths[0]),&(time_widths[0]),&(err_p2p_cool_min[0]),&(err_p2p_cool_max[0]));
mTemp_cool->SetTitle("Average cooling pipes temperatures");
rTemp_cool->SetTitle("Average cooling pipes RMS");

//TGraph *dTemp = new TGraph(4,setPoint_list, delta_temp);
//TGraphErrors *rTemp = new TGraphErrors(4,setPoint_list, mean_rms,0, rms_rms);

TCanvas *c1 = new TCanvas("c1","c1", 1200, 600);
//c1->Divide(1,2);
//c1->cd(1);
gPad->SetGridx();
gStyle->SetOptTitle(0);
mTemp->SetMarkerStyle(20);
mTemp->SetFillColor(kRed+2);
mTemp->Draw("AI2");
mTemp->GetXaxis()->SetTimeDisplay(1);
mTemp->GetXaxis()->SetNdivisions(720);
mTemp->GetYaxis()->SetTickLength(0.01);
mTemp->Draw("I2");
mTemp->GetXaxis()->SetLabelSize(0.035);
mTemp->GetYaxis()->SetLabelSize(0.035);
mTemp->GetXaxis()->SetTitle("Date");
mTemp->GetYaxis()->SetTitle("Temperature [#circC]");
mTemp->GetXaxis()->SetTitleSize(0.045);
mTemp->GetYaxis()->SetTitleOffset(0.6);
mTemp->GetYaxis()->SetTitleSize(0.045);
mTemp->GetXaxis()->SetTimeFormat("%b-%d %Hh %F1970-01-01 00:00:00");
//TLegend *leg_1 = new TLegend(0.15,0.7,0.35,0.9);
//leg_1->AddEntry(rTemp,"RMS distribution","F");
//leg_1->AddEntry(pTemp,"Peak to Peak excursion","F");
//leg_1->Draw();

//c1->cd(1);
TLatex *lx1 = new TLatex();
lx1->SetNDC(kFALSE);
lx1->SetTextSize(0.04);
lx1->SetTextAlign(12);
lx1->SetTextAngle(90);
for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
	double time = ((*it).stop_time + (*it).start_time)/2;
	//std::cout<<"time "<<time<<std::endl;
	
	lx1->DrawText(time,(*it).mean_temperature +1.3,((*it).run_number).c_str());
}
//lx1->Draw();

//c1->cd(2);
TCanvas *c2 = new TCanvas("c2","c2", 1200, 600);
//c2->Divide(1,2);
c2->cd();
//c1->cd(2);
//TLatex *lx2 = new TLatex();
//lx2->SetNDC(kFALSE);
//lx2->SetTextSize(0.04);
//lx2->SetTextAlign(12);
//lx2->SetTextAngle(90);
//for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
//	double time = ((*it).stop_time + (*it).start_time)/2;
	
//	lx2->DrawText(time,(*it).mean_rms +0.003,((*it).run_number).c_str());
//}
//lx2->Draw();
gPad->SetGridx();
rTemp->SetMarkerStyle(22);
rTemp->SetFillColor(kRed+2);
rTemp->Draw("AI2");
rTemp->GetXaxis()->SetTimeDisplay(1);
rTemp->GetXaxis()->SetNdivisions(720);
rTemp->GetYaxis()->SetTickLength(0.01);
rTemp->Draw("I2");
//rTemp->GetXaxis()->SetLabelSize(0.055);
//rTemp->GetYaxis()->SetLabelSize(0.055);
rTemp->GetXaxis()->SetTitle("Date");
rTemp->GetYaxis()->SetTitle("Temperature Variation [K]");
rTemp->GetYaxis()->SetTitleOffset(0.6);
rTemp->GetXaxis()->SetTitleSize(0.045);
rTemp->GetYaxis()->SetTitleSize(0.045);
rTemp->GetXaxis()->SetTimeFormat("%b-%d %Hh %F1970-01-01 00:00:00");
rTemp->GetYaxis()->SetTitleOffset(0.7);
pTemp->SetFillColor(kRed-10);
//pTemp->SetFillStyle(3002);
pTemp->Draw("I2");
rTemp->GetYaxis()->SetRangeUser(0,0.5);
rTemp->Draw("I2");
TCanvas *c3 = new TCanvas("c3","c3", 1200, 600);
//*/
c3->cd();
gPad->SetGridx();
rTemp_cool->SetMarkerStyle(22);
rTemp_cool->SetFillColor(kAzure+8);
//rTemp_cool->SetFillStyle(3004);
rTemp_cool->Draw("AI2");
rTemp_cool->GetXaxis()->SetTimeDisplay(1);
rTemp_cool->GetXaxis()->SetNdivisions(720);
rTemp_cool->GetYaxis()->SetTickLength(0.01);
rTemp_cool->Draw("I2");
//rTemp_cool->GetXaxis()->SetLabelSize(0.055);
//rTemp_cool->GetYaxis()->SetLabelSize(0.055);
rTemp_cool->GetXaxis()->SetTitle("Date");
rTemp_cool->GetYaxis()->SetTitle("Temperature Variation [K]");
rTemp_cool->GetYaxis()->SetTitleOffset(0.6);
rTemp_cool->GetXaxis()->SetTitleSize(0.045);
rTemp_cool->GetYaxis()->SetTitleSize(0.045);
rTemp_cool->GetXaxis()->SetTimeFormat("%b-%d %Hh %F1970-01-01 00:00:00");
rTemp_cool->GetYaxis()->SetTitleOffset(0.7);
pTemp_cool->SetFillColor(kBlue-5);
//pTemp_cool->SetFillStyle(3004);
pTemp_cool->Draw("sameI2");
rTemp_cool->Draw("I2");
c2->cd();
TLatex *lx1_cool = new TLatex();
lx1_cool->SetNDC(kFALSE);
lx1_cool->SetTextSize(0.04);
lx1_cool->SetTextAlign(12);
lx1_cool->SetTextAngle(90);
for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
	double time = ((*it).stop_time + (*it).start_time)/2;
	std::cout<<"time "<<time<<std::endl;
	
	lx1_cool->DrawText(time,(*it).mean_rms +0.003,((*it).run_number).c_str());
}
//lx1_cool->Draw();

c3->cd();
TLatex *lx2_cool = new TLatex();
lx2_cool->SetNDC(kFALSE);
lx2_cool->SetTextSize(0.04);
lx2_cool->SetTextAlign(12);
lx2_cool->SetTextAngle(90);
for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
	double time = ((*it).stop_time + (*it).start_time)/2;
	
	lx2_cool->DrawText(time,(*it).mean_rms_cool +0.003,((*it).run_number).c_str());
}
//lx2_cool->Draw();

std::vector<double> vTemp, vTime;
ifstream sp_stream;
sp_stream.open(setpoint_file.c_str());
	if(sp_stream.is_open()){
		std::stringstream spss;
		spss << sp_stream.rdbuf();
		double sp_temp;
		std::string timestamp, descriptor, date, hours;
		double startTime, stopTime;
		while(spss){
			spss>>descriptor;
			spss>>sp_temp;
			spss>>date;
			spss>>hours;
		
			timestamp = date +"_" +hours.substr(0,8);

			struct tm tm;
			time_t epoch;
			if ( strptime(timestamp.c_str(), "%d-%m-%Y_%H:%M:%S", &tm) != NULL ){
				//epoch = mktime(&tm);
				epoch = timegm(&tm);
				if(vTime.size() == 0 ) startTime = epoch;
				stopTime = epoch;
				vTemp.push_back(sp_temp);
				vTime.push_back(epoch);
			}
		
		}
		c1->cd();
		TGraph* sP_g = new TGraph(vTime.size(), &(vTime[0]), &(vTemp[0]));
		sP_g->RemovePoint(sP_g->GetN()-1);
		//sP_g->RemovePoint(sP_g->GetN());
		//sP_g->RemovePoint(sP_g->GetN());
		sP_g->SetLineStyle(2);
		mTemp->GetYaxis()->SetRangeUser(-25,25);
		gPad->SetGridx();
		mTemp_cool->SetMarkerStyle(20);
		mTemp_cool->SetFillColor(kAzure+8);
		mTemp_cool->Draw("I2");

		TLegend *ov_leg = new TLegend(0.6,0.6,0.9,0.75);
		ov_leg->SetFillStyle(0);
		ov_leg->SetBorderSize(0);
		sP_g->Draw("L");
		ov_leg->AddEntry(mTemp,"Average Module Temperature","F");
		ov_leg->AddEntry(mTemp_cool,"Average Cooling Pipe Temperature","F");
		ov_leg->Draw();
		drawAtlasInternal1D(0.67,0.8,0.105);
		c2->cd();
//		sP_g->Draw("l");
		c1->cd(2);
		drawAtlasInternal1D(0.67,0.8,0.105);
		double l_time[2] = {startTime-100000,stopTime};
		double l_value[2] = {0.05, 0.05};
		double l2_value[2] = {0.2, 0.2};
		double l2m_value[2] = {0.25, 0.25};
		TGraph* limit = new TGraph(2, l_time, l_value);
		TGraph* lmt = new TGraph(2, l_time, l_value);
		TGraph* limit2 = new TGraph(2, l_time, l2_value);
		TGraph* lmt2 = new TGraph(2, l_time, l2m_value);
		limit->SetLineStyle(2);	
		limit->SetLineColor(kAzure+8);
		lmt->SetLineStyle(2);	
		lmt->SetLineColor(kRed+2);
		limit2->SetLineStyle(2);	
		limit2->SetLineColor(kBlue+2);
		lmt2->SetLineStyle(2);	
		lmt2->SetLineColor(kRed);
		//lmt->Draw("l");
		//lmt2->Draw("l");
		//drawAtlasInternal1D();
		drawAtlasInternal1D(0.67,0.8,0.105);
		c3->cd();
		//limit->Draw("l");
		//limit2->Draw("l");
		rTemp_cool->GetYaxis()->SetRangeUser(0,0.3);

		TLegend *leg = new TLegend(0.1,0.65,0.5,0.85);
		leg->SetFillStyle(0);
		leg->SetBorderSize(0);
		leg->AddEntry(rTemp_cool,"Cool. Pipe Temperature RMS distribution","F");
		leg->AddEntry(pTemp_cool,"Cool. Pipe Peak to Peak excursion","F");
		leg->Draw();
		drawAtlasInternal1D(0.67,0.8,0.105);
		//drawAtlasInternal1D();
		c2->cd();
		TLegend *leg_1 = new TLegend(0.1,0.65,0.5,0.85);
		leg_1->SetFillStyle(0);
		leg_1->SetBorderSize(0);
		leg_1->AddEntry(rTemp,"Module Temperature RMS distribution","F");
		leg_1->AddEntry(pTemp,"Module TemperaturePeak to Peak excursion","F");
		leg_1->Draw();
		gPad->SetLeftMargin(0.07);
		gPad->SetRightMargin(0.01);
		c3->cd();
		gPad->SetLeftMargin(0.07);
		gPad->SetRightMargin(0.01);
		c1->cd();
		gPad->SetLeftMargin(0.07);
		gPad->SetRightMargin(0.01);
			
	}
/*	
	TCanvas *c3 = new TCanvas("c3","c3",1200,600);
	//module_p2p_stack
	c3->Divide(2,1);
	c3->cd(1);
	module_p2p_stack->Draw("");
	gPad->SetLogx();
	gPad->SetLogy();
//	cool_p2p_stack->SetFillColor(kAzure+8);	
	c3->cd(2);
	cool_p2p_stack->Draw("");	
	gPad->SetLogx();
	gPad->SetLogy();
*/
	c1->Print("AvgTemp.eps");
	c2->Print("AvgRMS.eps");
	c3->Print("AvgRMScool.eps");
}

void drawAtlasInternal1D(float xmin, float ymax, float margin) {
  //write "ATLAS Internal" label on 1D plots
  //
  //position
  //float xmin, xmax, ymin, ymax;
  //xmin=0.75; xmax=0.85; ymin=0.70; ymax=0.80;
  //ATLAS
  TLatex a;
  a.SetNDC();
  a.SetTextFont(72);
  a.SetTextColor(1);
  a.SetTextSize(0.07);
 	a.DrawLatex(xmin,ymax,"ATLAS");
  //
 	//internal
  TLatex i;
  i.SetNDC();
  i.SetTextFont(42);
  i.SetTextColor(1);
  i.SetTextSize(0.07);
	i.DrawLatex(xmin+margin,ymax,"Internal");//Preliminary");
}
                                  
