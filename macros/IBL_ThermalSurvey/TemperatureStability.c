#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "TGraph.h"
#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include <string>
#include <sstream>
#include <fstream>
#include "TMultiGraph.h"
#include "TPad.h"
#include "TLegend.h"
#include "THStack.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TColor.h"
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

struct cooling_data{
	bool outlet;
	std::string cool_side;
	std::vector<double> temps;
	std::vector<double> temp_time;
	TH1D* temp_distro;
	TGraph* temp_history;
};

struct module_data{
	std::string module_temperature;
	std::string module_power;
	std::string module_hv_v;
	std::string module_hv_i;
	std::vector<double> temps;
	std::vector<double> temp_time;
	std::vector<double> powers;
	std::vector<double> power_time;
	std::vector<double> hv_v;
	std::vector<double> hv_v_time;
	std::vector<double> hv_i;
	std::vector<double> hv_i_time;

	TH1D* temp_distro;
	TGraph* temp_history;
	TH1D* power_distro;
	TGraph* power_history;

	TH1D* hv_v_distro;
	TGraph* hv_v_history;
	TH1D* hv_i_distro;
	TGraph* hv_i_history;
};

struct run_time_interval{
	int run_number;
	double start_time;
	double stop_time;

};



//void module_data_constructor(module_data MD, std::string module_name);
//std::string module_indextoname(int i);
double corr_cooling(double temp);
//void set_point_graph(std::string set_point, run_time_interval rti);
void fill_temp_map(TH2D* map, double temp, std::string module, bool module_check = true);
void draw_option_temp_map(TH2D* map);
//double dayTime_in_sec(std::string dayTime);
//unsigned long int date_in_days(std::string date);
void fill_time_interval(int run_number, run_time_interval* RTI, std::string run_list_filname);
void select_data(run_time_interval RTI, std::stringstream& buffer, std::stringstream& selection);
void fill_power_map(TH2D* map, double power, std::string module);
void TemperatureStabilityForRun(int runNumber, std::string temp_file_name, std::string power_file_name, std::string cool_file_name, std::string hv_v_file_name, std::string hv_i_file_name, std::string run_list);
void sort_module_data(std::stringstream& temp_buffer,std::stringstream& power_buffer,std::stringstream& hv_v_buffer, std::stringstream& hv_i_buffer,  module_data* MD);


double corr_cooling(double temp){
	double corr =  (4e-8*pow(temp,4))-(2e-06*pow(temp,3))+(5e-04*pow(temp,2))-(0.0487*temp)+1.1974;
	//std::cout<<"TEMP: "<<temp<<"\tCORRECTION IS :"<<corr<<std::endl;
	return corr;

}

void fill_power_map(TH2D* map, double power, std::string module){
	int stave_id = atoi(module.substr(4,2).c_str());
	if(module.find("C_M4") != std::string::npos) map->SetBinContent(1,stave_id,power);
	if(module.find("C_M3") != std::string::npos) map->SetBinContent(2,stave_id,power);
	if(module.find("C_M2") != std::string::npos) map->SetBinContent(3,stave_id,power);
	if(module.find("C_M1") != std::string::npos) map->SetBinContent(4,stave_id,power);
	if(module.find("A_M1") != std::string::npos) map->SetBinContent(5,stave_id,power);
	if(module.find("A_M2") != std::string::npos) map->SetBinContent(6,stave_id,power);
	if(module.find("A_M3") != std::string::npos) map->SetBinContent(7,stave_id,power);
	if(module.find("A_M4") != std::string::npos) map->SetBinContent(8,stave_id,power);
}

void TempStability(std::string week_prefix){
//std::string temp_file_name, std::string power_file_name, std::string cool_file_name, std::string run_list, std::string set_point){
	std::string temp_file_name = week_prefix + "_mod_temperature.txt";
	std::string power_file_name = week_prefix + "_power.txt";
	std::string cool_file_name = week_prefix + "_cool.txt";
	std::string hv_v_file_name = week_prefix + "_hv_V.txt";
	std::string hv_i_file_name = week_prefix + "_hv_I.txt";
	std::string run_list = week_prefix + "_runQuery.txt";

	std::vector<int> runs;
	std::string trash;	
	int run;

	ifstream runListFile;
	runListFile.open(run_list.c_str());
	std::stringstream buffer;
	buffer << runListFile.rdbuf();
	while(buffer){
		buffer>>run;
		buffer>>trash;
		buffer>>trash;
		runs.push_back(run);
		std::cout<<run<<std::endl;
	} 
	runListFile.close();
	
	for(std::vector<int>::iterator it = runs.begin(); it != runs.end(); ++it) {
//     std::cout << *it; ... 
		TemperatureStabilityForRun(*it, temp_file_name, power_file_name, cool_file_name, hv_v_file_name, hv_i_file_name, run_list);
	}	
}

void draw_option_power_map(TH2D* power_map){
	power_map->Draw();

	std::string labels[8] = {"M4C","M3C","M2C","M1C","M1A","M2A","M3A","M4A"};
	for(int i=1; i<=8; i++)power_map->GetXaxis()->SetBinLabel(i,labels[i-1].c_str());
	power_map->GetXaxis()->SetTitle("Sector");
	power_map->GetYaxis()->SetTitle("Stave");
	power_map->GetXaxis()->SetLabelSize(0.060);
	power_map->GetYaxis()->SetLabelSize(0.060);
	power_map->GetXaxis()->SetTitleSize(0.060);
	power_map->GetYaxis()->SetTitleSize(0.060);
	power_map->GetXaxis()->SetTitleOffset(0.8);
}

void draw_option_temp_map(TH2D* temp_map){
	temp_map->Draw();

	std::string labels[10] = {"Cool_C","M4C","M3C","M2C","M1C","M1A","M2A","M3A","M4A","Cool_A"};
	for(int i=1; i<=10; i++)temp_map->GetXaxis()->SetBinLabel(i,labels[i-1].c_str());
	temp_map->GetXaxis()->SetTitle("Sector");
	temp_map->GetYaxis()->SetTitle("Stave");
	temp_map->GetXaxis()->SetLabelSize(0.060);
	temp_map->GetYaxis()->SetLabelSize(0.060);
	temp_map->GetXaxis()->SetTitleSize(0.060);
	temp_map->GetYaxis()->SetTitleSize(0.060);
	temp_map->GetXaxis()->SetTitleOffset(0.8);

}

void fill_temp_map(TH2D* map, double temp, std::string module, bool module_check){
	int stave_id = atoi(module.substr(4,2).c_str());
	//std::cout<<"MODULE "<<module<<std::endl;
	if(module.find("_C") != std::string::npos && !module_check) map->SetBinContent(1,stave_id, temp);	
	if(module.find("C_M4") != std::string::npos) map->SetBinContent(2,stave_id,temp);
	if(module.find("C_M3") != std::string::npos) map->SetBinContent(3,stave_id,temp);
	if(module.find("C_M2") != std::string::npos) map->SetBinContent(4,stave_id,temp);
	if(module.find("C_M1") != std::string::npos) map->SetBinContent(5,stave_id,temp);
	if(module.find("A_M1") != std::string::npos) map->SetBinContent(6,stave_id,temp);
	if(module.find("A_M2") != std::string::npos) map->SetBinContent(7,stave_id,temp);
	if(module.find("A_M3") != std::string::npos) map->SetBinContent(8,stave_id,temp);
	if(module.find("A_M4") != std::string::npos) map->SetBinContent(9,stave_id,temp);
	if(module.find("_A") != std::string::npos && !module_check) map->SetBinContent(10,stave_id, temp);	

}

void fill_time_interval(int run_number, run_time_interval* RTI, std::string run_list_filename){
	//RTI.run_number = run_number;
	std::string rNumber, rStop, rStart;

	ifstream run_list;
	run_list.open(run_list_filename.c_str());

	if(run_list.is_open()){
		std::stringstream buffer;
		buffer << run_list.rdbuf();
			
		while(buffer){
		//	std::getline(buffer, rNumber, ',');
		//	std::getline(buffer, rStart, ',');
		//	std::getline(buffer, rStop);
		buffer>>rNumber;
		buffer>>rStart;
		buffer>>rStop;
			if(atoi(rNumber.c_str()) == run_number){
				RTI->run_number = atoi(rNumber.c_str());
				RTI->start_time = atof(rStart.c_str());
				RTI->stop_time = atof(rStop.c_str());
				//std::cout<<run_number<<"\t"<<RTI->run_number<<"\t"<<RTI->start_time<<"\t"<<RTI->stop_time<<std::endl;
			} 
		}
	}
}

void select_data(run_time_interval RTI, std::stringstream& buffer, std::stringstream& selection){
//	std::stringstream selection;
	selection.str(std::string());
	std::string module, date, hours, timestamp;
	double data;
	while(buffer){
		buffer>>module;
		buffer>>data;		
		buffer>>date;
		buffer>>hours;
		//buffer>>data;		

		timestamp = date +"_" +hours.substr(0,8);

		struct tm tm;
		time_t epoch;
		if ( strptime(timestamp.c_str(), "%d-%m-%Y_%H:%M:%S", &tm) != NULL ){
			//epoch = mktime(&tm);
			epoch = timegm(&tm);

			if(epoch>=RTI.start_time && epoch<=RTI.stop_time){
				selection<<module<<"\t"<<epoch<<"\t"<<data<<std::endl;
				//std::cout<<timestamp<<"\t"<<epoch<<std::endl;
			}
		}
	}
}

void sort_cooling_data(std::stringstream& buffer, cooling_data* CD){
	int buffer_counter = 0;
	std::string module;
	int time;
	double temperature;

	int stave_id, stave_side_offset, module_id, m_index=0;
	buffer.seekg(0,ios::beg);

	while(!buffer.eof()){
		buffer_counter++;
		buffer>>module;
		//std::cout<<"***** "<<module<<std::endl;	
		stave_id = atoi(module.substr(4,2).c_str());
		if(module.substr(7,1)=="A")stave_side_offset=0;
		else stave_side_offset=1;	
		module_id = atoi(module.substr(10,1).c_str());
		m_index = (stave_id-1)*2+stave_side_offset;
		if(stave_id%2 == 0 && stave_side_offset == 0) CD[m_index].outlet = true;
		else if(stave_id%2 == 1 && stave_side_offset == 1 ) CD[m_index].outlet = true;
		else CD[m_index].outlet = false;
		buffer>>time;
		buffer>>temperature;
		temperature = temperature - corr_cooling(temperature);


		if(CD[m_index].temps.size()==0){
			CD[m_index].temp_distro = new TH1D(module.c_str(), module.c_str(), 1000, -30, 30);

			//if(m_index!=0){
			//	std::cout<<"M_INDEX: "<<m_index<<std::endl;
			//	CD[m_index-1].temp_history = new TGraph(CD[m_index-1].temps.size(),&(CD[m_index-1].temp_time[0]),&(CD[m_index-1].temps[0]));
			//	CD[m_index-1].temp_history->SetTitle(CD[m_index-1].cool_side.c_str());
			//	CD[m_index-1].temp_history->SetName(CD[m_index-1].cool_side.c_str());
			//}
		}
		if(CD[m_index].temp_time.size()>0){
			if(time != CD[m_index].temp_time.back()){
				CD[m_index].temps.push_back(temperature);
				CD[m_index].temp_time.push_back(time);
			//temps.push_back(temperature);
			//temp_time.push_back(time);
				CD[m_index].cool_side = module;
				CD[m_index].temp_distro->Fill(temperature);
			}
		}else{
				CD[m_index].temps.push_back(temperature);
				CD[m_index].temp_time.push_back(time);
			//temps.push_back(temperature);
			//temp_time.push_back(time);
				CD[m_index].cool_side = module;
				CD[m_index].temp_distro->Fill(temperature);
		}
		//std::cout<<CD[m_index].cool_side<<" "<<m_index<<"TIME: "<<time<<"TEMPERATURE: "<<temperature<<"MODULE: "<<module<<std::endl;	
	}

//	CD[m_index].temp_history = new TGraph(CD[m_index].temps.size(),&(CD[m_index].temp_time[0]),&(CD[m_index].temps[0]));
//	CD[m_index].temp_history->SetTitle(CD[m_index].cool_side.c_str());
//	CD[m_index].temp_history->SetName(CD[m_index].cool_side.c_str());

	for(int index = 0; index < 28; index++){
		CD[index].temp_history = new TGraph(CD[index].temps.size(),&(CD[index].temp_time[0]),&(CD[index].temps[0]));
		CD[index].temp_history->SetTitle(CD[index].cool_side.c_str());
		CD[index].temp_history->SetName(CD[index].cool_side.c_str());
	}
	std::cout<<"DONE WITH SORT COOLING DATA: "<<CD[27].cool_side<<std::endl;
}

void sort_module_data(std::stringstream& buffer, std::stringstream& buffer_power, std::stringstream& buffer_hv_v, std::stringstream& buffer_hv_i, module_data* MD){
	int buffer_counter = 0;
	std::string module, module2, module3, module4;
	int time;
	double temperature, power, hv_v, hv_i;

	

	int stave_id, stave_side_offset, module_id, m_index=0;
	buffer.seekg(0,ios::beg);

	while(!buffer.eof()){
		buffer_counter++;
		buffer>>module;	
		stave_id = atoi(module.substr(4,2).c_str());
		if(module.substr(7,1)=="A")stave_side_offset=0;
		else stave_side_offset=4;	
		module_id = atoi(module.substr(10,1).c_str());
		m_index = (stave_id-1)*8+stave_side_offset+(module_id-1);
		buffer>>time;
		buffer>>temperature;
		if(MD[m_index].temps.size()==0){
			MD[m_index].temp_distro = new TH1D(module.c_str(), module.c_str(), 1000, -30, 30);

			if(m_index!=0){
				MD[m_index-1].temp_history = new TGraph(MD[m_index-1].temps.size(),&(MD[m_index-1].temp_time[0]),&(MD[m_index-1].temps[0]));
				MD[m_index-1].temp_history->SetTitle(MD[m_index-1].module_temperature.c_str());
				MD[m_index-1].temp_history->SetName(MD[m_index-1].module_temperature.c_str());
			}
		}
		MD[m_index].temps.push_back(temperature);
		MD[m_index].temp_time.push_back(time);
		//temps.push_back(temperature);
		//temp_time.push_back(time);
		MD[m_index].module_temperature = module;
		MD[m_index].temp_distro->Fill(temperature);
		//std::cout<<MD[m_index].module_temperature<<std::endl;	
	}

	MD[m_index].temp_history = new TGraph(MD[m_index].temps.size(),&(MD[m_index].temp_time[0]),&(MD[m_index].temps[0]));
	MD[m_index].temp_history->SetTitle(MD[m_index].module_temperature.c_str());
	MD[m_index].temp_history->SetName(MD[m_index].module_temperature.c_str());

	buffer_power.seekg(0,ios::beg);
	while(!buffer_power.eof()){
		buffer_power>>module2;	
		stave_id = atoi(module2.substr(4,2).c_str());
		if(module2.substr(7,1)=="A")stave_side_offset=0;
		else stave_side_offset=4;	
		module_id = atoi(module2.substr(10,1).c_str());
		m_index = (stave_id-1)*8+stave_side_offset+(module_id-1);
		buffer_power>>time;
		buffer_power>>power;
		if(MD[m_index].powers.size()==0){
			MD[m_index].power_distro = new TH1D(module2.c_str(), module2.c_str(), 100, 0, 10);

			if(m_index!=0){
				//std::cout<<module2<<std::endl;
				MD[m_index-1].power_history = new TGraph(MD[m_index-1].powers.size(),&(MD[m_index-1].power_time[0]),&(MD[m_index-1].powers[0]));
				MD[m_index-1].power_history->SetTitle(MD[m_index-1].module_power.c_str());
				MD[m_index-1].power_history->SetName(MD[m_index-1].module_power.c_str());
			//	MD[m_index-1].temp_history->Draw("AL");
			}
		}
			
		MD[m_index].powers.push_back(power);
		MD[m_index].power_time.push_back(time);
		//temps.push_back(temperature);
		//temp_time.push_back(time);
		MD[m_index].module_power = module2;
		MD[m_index].power_distro->Fill(power);
	}
	//std::cout<<MD[m_index].module_power<<"\t"<<MD[m_index].powers.size()<<std::endl;
	MD[m_index].power_history = new TGraph(MD[m_index].powers.size(),&(MD[m_index].power_time[0]),&(MD[m_index].powers[0]));
	MD[m_index].power_history->SetTitle(MD[m_index].module_power.c_str());
	MD[m_index].power_history->SetName(MD[m_index].module_power.c_str());


/*	buffer_hv_v.seekg(0,ios::beg);
	while(!buffer_hv_v.eof()){
		buffer_hv_v>>module3;	
		stave_id = atoi(module3.substr(4,2).c_str());
		if(module3.substr(7,1)=="A")stave_side_offset=0;
		else stave_side_offset=4;	
		module_id = atoi(module3.substr(10,1).c_str());
		m_index = (stave_id-1)*8+stave_side_offset+(module_id-1);
		buffer_hv_v>>time;
		buffer_hv_v>>hv_v;
		if(MD[m_index].hv_v.size()==0){
			MD[m_index].hv_v_distro = new TH1D(module3.c_str(), module3.c_str(), 1000, -300, 0);
			if(m_index!=0){
				//std::cout<<module2<<std::endl;
				MD[m_index-1].hv_v_history = new TGraph(MD[m_index-1].hv_v.size(),&(MD[m_index-1].hv_v_time[0]),&(MD[m_index-1].hv_v[0]));
				MD[m_index-1].hv_v_history->SetTitle(MD[m_index-1].module_hv_v.c_str());
				MD[m_index-1].hv_v_history->SetName(MD[m_index-1].module_hv_v.c_str());
			//	MD[m_index-1].temp_history->Draw("AL");
			}
		}
			
		MD[m_index].hv_v.push_back(hv_v);
		MD[m_index].hv_v_time.push_back(time);
		MD[m_index].module_hv_v = module3;
		MD[m_index].hv_v_distro->Fill(hv_v);
	}
	std::cout<<MD[m_index].module_hv_v<<"\t"<<MD[m_index].hv_v.size()<<std::endl;
	MD[m_index].hv_v_history = new TGraph(MD[m_index].hv_v.size(),&(MD[m_index].hv_v_time[0]),&(MD[m_index].hv_v[0]));
	MD[m_index].hv_v_history->SetTitle(MD[m_index].module_hv_v.c_str());
	MD[m_index].hv_v_history->SetName(MD[m_index].module_hv_v.c_str());


	buffer_hv_i.seekg(0,ios::beg);
	while(!buffer_hv_i.eof()){
		//std::cout<<buffer_hv_i.str()<<std::endl;
		buffer_hv_i>>module4;	
		//std::cout<<"cazzo"<<module4<<std::endl;
		stave_id = atoi(module4.substr(4,2).c_str());
		if(module4.substr(7,1)=="A")stave_side_offset=0;
		else stave_side_offset=4;	
		module_id = atoi(module4.substr(10,1).c_str());
		m_index = (stave_id-1)*8+stave_side_offset+(module_id-1);
		buffer_hv_i>>time;
		buffer_hv_i>>hv_i;
		if(MD[m_index].hv_i.size()==0){
			MD[m_index].hv_i_distro = new TH1D(module4.c_str(), module4.c_str(), 1000, -3e-3, 0);

			if(m_index!=0){
				//std::cout<<module2<<std::endl;
				MD[m_index-1].hv_i_history = new TGraph(MD[m_index-1].hv_i.size(),&(MD[m_index-1].hv_i_time[0]),&(MD[m_index-1].hv_i[0]));
				MD[m_index-1].hv_i_history->SetTitle(MD[m_index-1].module_hv_i.c_str());
				MD[m_index-1].hv_i_history->SetName(MD[m_index-1].module_hv_i.c_str());
			//	MD[m_index-1].temp_history->Draw("AL");
			}
		}
			
		MD[m_index].hv_i.push_back(hv_i);
		MD[m_index].hv_i_time.push_back(time);
		MD[m_index].module_hv_i = module4;
		MD[m_index].hv_i_distro->Fill(hv_i);
	}
	std::cout<<MD[m_index].module_hv_i<<"\t"<<MD[m_index].hv_i.size()<<std::endl;
	MD[m_index].hv_i_history = new TGraph(MD[m_index].hv_i.size(),&(MD[m_index].hv_i_time[0]),&(MD[m_index].hv_i[0]));
	MD[m_index].hv_i_history->SetTitle(MD[m_index].module_hv_i.c_str());
	MD[m_index].hv_i_history->SetName(MD[m_index].module_hv_i.c_str());
*/
}

void TemperatureStabilityForRun(int runNumber, std::string temp_file_name, std::string power_file_name, std::string cool_file_name, std::string hv_v_file_name, std::string hv_i_file_name, std::string run_list){

//	std::string sel_module = "LI_S01_A_M1_TModule";
	std::cout<<runNumber<<std::endl;
	std::cout<<temp_file_name<<std::endl;
	std::cout<<power_file_name<<std::endl;
	std::cout<<cool_file_name<<std::endl;
	std::cout<<hv_v_file_name<<std::endl;
	std::cout<<hv_i_file_name<<std::endl;
	
	module_data MD_sectors[112];
	cooling_data CD_sides[28];

	//std::string time_interval = "0703-1603";
	//std::string temp_file_name = "M7_mod_temperature.txt";
	//std::string run_list = "M7_runQuery.txt";
	ifstream ddv_data_temp;
	TH2D* temp_mean_map = new TH2D("temp_mean_map", "temp_mean_map", 10, 0, 10, 14, 0.5, 14.5);
	TH2D* temp_rms_map = new TH2D("temp_RMS_map", "temp_RMS_map", 10, 0, 10, 14, 0.5, 14.5);
	TH2D* temp_p2p_map = new TH2D("temp_p2p_map", "temp_p2p_map", 10, 0, 10, 14, 0.5, 14.5);
	TH2D* power_mean_map = new TH2D("power_mean_map", "power_mean_map", 8, 0, 8, 14, 0.5, 14.5);
	TH2D* power_rms_map = new TH2D("power_RMS_map", "power_RMS_map", 8, 0, 8, 14, 0.5, 14.5);
	TH2D* power_p2p_map = new TH2D("power_p2p_map", "power_p2p_map", 8, 0, 8, 14, 0.5, 14.5);
	TH1D* temp_mean_distro = new TH1D("temp_mean_distro", "temp_mean_distro", 180, -30, 30);
	TH1D* temp_rms_distro = new TH1D("temp_rms_distro", "temp_rms_distro", 1800, 0, 50);
	TH1D* temp_p2p_distro = new TH1D("temp_p2p_distro", "temp_p2p_distro", 1800, 0, 50);
	TH1D* power_mean_distro = new TH1D("power_mean_distro", "power_mean_distro", 180, 0, 30);
	TH1D* power_rms_distro = new TH1D("power_rms_distro", "power_rms_distro", 1800, 0, 50);
	TH1D* power_p2p_distro = new TH1D("power_p2p_distro", "power_p2p_distro", 1800, 0, 50);
	TH1D* cool_mean_distro_inlet = new TH1D("cool_mean_distro_inlet", "cool_mean_distro_inlet", 180, -30, 30);
	TH1D* cool_rms_distro_inlet = new TH1D("cool_rms_distro_inlet", "cool_rms_distro_inlet", 1800, 0, 50);
	TH1D* cool_p2p_distro_inlet = new TH1D("cool_p2p_distro_inlet", "cool_p2p_distro_inlet", 1800, 0, 50);
	TH1D* cool_mean_distro_outlet = new TH1D("cool_mean_distro_outlet", "cool_mean_distro_outlet", 180, -30, 30);
	TH1D* cool_rms_distro_outlet = new TH1D("cool_rms_distro_outlet", "cool_rms_distro_outlet", 1800, 0, 50);
	TH1D* cool_p2p_distro_outlet = new TH1D("cool_p2p_distro_outlet", "cool_p2p_distro_outlet", 1800, 0, 50);
	TH1D* cool_rms_list_inlet = new TH1D("cool_rms_list_inlet","cool_rms_list_inlet", 14, 0, 14);
	TH1D* cool_rms_list_outlet = new TH1D("cool_rms_list_outlet", "cool_rms_list_outlet", 14, 0, 14);
	TH1D* cool_p2p_list_inlet = new TH1D("cool_p2p_list_inlet", "cool_p2p_list_outlet", 14, 0, 14);
	TH1D* cool_p2p_list_outlet = new TH1D("cool_p2p_list_outlet", "cool_p2p_list_outlet", 14, 0, 14);
	TH1D* temp_rms_list = new TH1D("temp_rms_list", "temp_rms_list", 112,0,112);
	TH1D* temp_p2p_list = new TH1D("temp_PeakToPeak_list", "temp_PeakToPeak_list", 112,0,112);
	

	std::string new_dir = "/afs/cern.ch/user/a/amiucci/work/public/IBL_temp_monitoring/"+run_list.substr(0,run_list.size()-4)+"/";

	mkdir(new_dir.c_str(),0777);

	std::stringstream output_file_name;
	output_file_name<<new_dir<<runNumber<<".root";

  std::string ofn = output_file_name.str();
  TFile *output_file = new TFile(ofn.c_str(), "RECREATE");
	run_time_interval rti;

	fill_time_interval(runNumber, &rti, run_list);
	std::cout<<"RUN NUMBER: "<<rti.run_number<<std::endl;
	ifstream temp_file;
	temp_file.open(temp_file_name.c_str());
	if(temp_file.is_open()){
		std::stringstream temp_buffer;
		output_file->mkdir("ModulesTemperatures");
		output_file->cd("ModulesTemperatures");
		temp_buffer << temp_file.rdbuf();
		std::stringstream temp_selection;
		select_data(rti, temp_buffer, temp_selection);
	//	std::cout<<temp_selection.str();
		ifstream power_file;
		power_file.open(power_file_name.c_str());
	//	if(power_file.is_open()){
		std::stringstream power_buffer;
		power_buffer << power_file.rdbuf();
		std::stringstream power_selection;
		select_data(rti, power_buffer, power_selection);
	//	}

		ifstream hv_v_file;
		hv_v_file.open(hv_v_file_name.c_str());
		std::stringstream hv_v_buffer;
		hv_v_buffer << hv_v_file.rdbuf();
		std::stringstream hv_v_selection;
		select_data(rti, hv_v_buffer, hv_v_selection);
		//std::cout<<hv_v_selection.str()<<std::endl;	


		ifstream hv_i_file;
		hv_i_file.open(hv_i_file_name.c_str());
		std::stringstream hv_i_buffer;
		hv_i_buffer << hv_i_file.rdbuf();
		std::stringstream hv_i_selection;
		select_data(rti, hv_i_buffer, hv_i_selection);


		sort_module_data(temp_selection, power_selection, hv_v_selection, hv_i_selection, MD_sectors);
		for(int i=0; i < 112; i++){
			std::string folder = "ModulesTemperatures/"+MD_sectors[i].module_temperature.substr(0,(MD_sectors[i].module_temperature.size()-7));
			//std::cout<<folder<<std::endl;
			output_file->mkdir(folder.c_str());
			output_file->cd(folder.c_str());
			MD_sectors[i].temp_distro->Write();
			MD_sectors[i].power_distro->Write();
			//MD_sectors[i].hv_v_distro->Write();
			//MD_sectors[i].hv_i_distro->Write();
			//std::cout<<MD_sectors[i].temp_distro<<"\t"<<MD_sectors[i].temp_history<<std::endl;
			MD_sectors[i].temp_history->Write((MD_sectors[i].module_temperature).c_str());
			MD_sectors[i].power_history->Write((MD_sectors[i].module_power).c_str());
			//MD_sectors[i].hv_v_history->Write((MD_sectors[i].module_hv_v).c_str());
			//MD_sectors[i].hv_i_history->Write((MD_sectors[i].module_hv_i).c_str());

			temp_mean_distro->Fill(MD_sectors[i].temp_distro->GetMean());
			temp_rms_distro->Fill(MD_sectors[i].temp_distro->GetRMS());
			power_mean_distro->Fill(MD_sectors[i].power_distro->GetMean());
			power_rms_distro->Fill(MD_sectors[i].power_distro->GetRMS());
			temp_rms_list->SetBinContent(i+1,(double)MD_sectors[i].temp_distro->GetRMS());
			double t_min;// = MD_sectors[i].temp_history->GetHistogram()->GetMinimum();
			double t_max;// = MD_sectors[i].temp_history->GetHistogram()->GetMaximum();
			double p_min;// = MD_sectors[i].power_history->GetHistogram()->GetMinimum();
			double p_max;// = MD_sectors[i].power_history->GetHistogram()->GetMaximum();
			if(MD_sectors[i].temp_distro->GetRMS() > 0.000001){
				t_min = MD_sectors[i].temp_history->GetHistogram()->GetMinimum();
				t_max = MD_sectors[i].temp_history->GetHistogram()->GetMaximum();
			}else{
				std::cout<<"BA LA LA LA LA"<<std::endl;
				t_min = 0;
				t_max = 0;
			}

			if(MD_sectors[i].power_distro->GetRMS() > 0.000001 ){
				p_min = MD_sectors[i].power_history->GetHistogram()->GetMinimum();
				p_max = MD_sectors[i].power_history->GetHistogram()->GetMaximum();
			}else{
				p_min = 0;
				p_max = 0;
			}
			power_p2p_distro->Fill( (p_max-p_min));
			if(t_max-t_min <1.3 && t_max-t_min > 1.1){
				std::cout<<"BANG: "<<t_max<<" "<<t_min<<std::endl;
			}
			temp_p2p_distro->Fill( (t_max-t_min));
			//std::cout<<"MAX = "<<max<<"\tMIN = "<<min<<std::endl;
			temp_p2p_list->SetBinContent(i+1, (t_max-t_min));
			//temp_rms_list->SetBinLabel(i,MD_sectors[i].module_temperature);
			//temp_p2p_list->SetBinLabel(i,MD_sectors[i].module_temperature);
			fill_temp_map(temp_mean_map, MD_sectors[i].temp_distro->GetMean(), MD_sectors[i].module_temperature);
			fill_temp_map(temp_rms_map, MD_sectors[i].temp_distro->GetRMS(), MD_sectors[i].module_temperature);
			fill_temp_map(temp_p2p_map,  (t_max-t_min), MD_sectors[i].module_temperature);
			fill_power_map(power_mean_map, MD_sectors[i].power_distro->GetMean(), MD_sectors[i].module_power);
			fill_power_map(power_rms_map, MD_sectors[i].power_distro->GetRMS(), MD_sectors[i].module_power);
			fill_power_map(power_p2p_map,  (p_max-p_min), MD_sectors[i].module_power);
		}//	std::cout<<MD_sectors[i].module<<std::endl;
		output_file->cd();

		ifstream cool_file;
		cool_file.open(cool_file_name.c_str());
		std::stringstream cool_buffer;
		cool_buffer << cool_file.rdbuf();
		std::stringstream cool_selection;
		select_data(rti, cool_buffer, cool_selection);	

		sort_cooling_data(cool_selection, CD_sides);
		std::string cool_folder = "CoolingTemperatures/";
		output_file->mkdir(cool_folder.c_str());
		output_file->cd(cool_folder.c_str());

		std::cout<<"about to enter in the writing process"<<std::endl;
		for(int i = 0; i<28; i++){
			CD_sides[i].temp_distro->Write();
			std::cout<<"temp distro Written "<<i<<std::endl;
			CD_sides[i].temp_history->Write(CD_sides[i].cool_side.c_str());
			double c_max; // = CD_sides[i].temp_history->GetHistogram()->GetMaximum();
			double c_min; // = CD_sides[i].temp_history->GetHistogram()->GetMinimum();
	
			if(CD_sides[i].temp_distro->GetRMS() > 0.00000001){
				c_max =  CD_sides[i].temp_history->GetHistogram()->GetMaximum();
				c_min = CD_sides[i].temp_history->GetHistogram()->GetMinimum();
			}else{
				c_max = 0;
				c_min = 0;
			}

			if(CD_sides[i].outlet){
				cool_mean_distro_outlet->Fill(CD_sides[i].temp_distro->GetMean());
				cool_rms_distro_outlet->Fill(CD_sides[i].temp_distro->GetRMS());
				cool_p2p_distro_outlet->Fill( (c_max-c_min));
				cool_rms_list_outlet->SetBinContent((i/2)+1, CD_sides[i].temp_distro->GetRMS());
				cool_p2p_list_outlet->SetBinContent((i/2)+1,  (c_max-c_min));
			}else{
				cool_mean_distro_inlet->Fill(CD_sides[i].temp_distro->GetMean());
				cool_rms_distro_inlet->Fill(CD_sides[i].temp_distro->GetRMS());
				cool_p2p_distro_inlet->Fill( (c_max-c_min));
				cool_rms_list_inlet->SetBinContent((i/2)+1, CD_sides[i].temp_distro->GetRMS());
				cool_p2p_list_inlet->SetBinContent((i/2)+1,  (c_max-c_min));
			}
			//std::cout<<"temp history Written "<<i<<std::endl;
			fill_temp_map(temp_mean_map, CD_sides[i].temp_distro->GetMean(), CD_sides[i].cool_side, false);
			fill_temp_map(temp_rms_map, CD_sides[i].temp_distro->GetRMS(), CD_sides[i].cool_side, false);
			fill_temp_map(temp_p2p_map,  (c_max-c_min), CD_sides[i].cool_side, false);
		}
		std::cout<<"..."<<std::endl;
		output_file->cd();
		//std::cout<<"about to draw"<<std::endl;
		draw_option_temp_map(temp_mean_map);
		draw_option_temp_map(temp_rms_map);
		draw_option_temp_map(temp_p2p_map);
		draw_option_power_map(power_mean_map);
		draw_option_power_map(power_rms_map);
		draw_option_power_map(power_p2p_map);
		//std::cout<<"about to write the distros"<<std::endl;
		temp_mean_distro->Write();
		temp_rms_distro->Write();
		temp_p2p_distro->Write();
		power_mean_distro->Write();
		power_rms_distro->Write();
		cool_mean_distro_inlet->Write();
		cool_rms_distro_inlet->Write();
		cool_mean_distro_outlet->Write();
		cool_rms_distro_outlet->Write();
		cool_p2p_distro_inlet->Write();	
		cool_p2p_distro_outlet->Write();
		temp_rms_list->Draw();
		temp_p2p_list->Draw();	
		for(int i=0; i<112; i++){
			temp_rms_list->GetXaxis()->SetBinLabel(i+1,(MD_sectors[i].module_temperature.substr(0,11)).c_str());
			temp_p2p_list->GetXaxis()->SetBinLabel(i+1,(MD_sectors[i].module_temperature.substr(0,11)).c_str());	
		}
		cool_p2p_list_inlet->Draw();
		cool_p2p_list_outlet->Draw();
		cool_rms_list_inlet->Draw();
		cool_rms_list_outlet->Draw();
		for(int i=0; i<28; i++){
			if(CD_sides[i].outlet){
				cool_rms_list_outlet->GetXaxis()->SetBinLabel((i/2)+1, CD_sides[i].cool_side.substr(0.11).c_str());
				cool_p2p_list_outlet->GetXaxis()->SetBinLabel((i/2)+1, CD_sides[i].cool_side.substr(0.11).c_str());
			}else{
				cool_rms_list_inlet->GetXaxis()->SetBinLabel((i/2)+1, CD_sides[i].cool_side.substr(0.11).c_str());
				cool_p2p_list_inlet->GetXaxis()->SetBinLabel((i/2)+1, CD_sides[i].cool_side.substr(0.11).c_str());
			}
		}
		cool_rms_list_inlet->Write();
		cool_rms_list_outlet->Write();
		cool_p2p_list_inlet->Write();
		cool_p2p_list_outlet->Write();
		temp_rms_list->Write();
		temp_p2p_list->Write();
		temp_mean_map->Write();
		temp_rms_map->Write();
		temp_p2p_map->Write();
		power_mean_map->Write();
		power_rms_map->Write();
		power_p2p_map->Write();
		

		//std::cout<<"SET POINT GRAPH"<<std::endl;	

		//set_point_graph(set_point, rti);
		output_file->Close();
		//std::cout<<"FILE CLOSED"<<std::endl;
	}
}

/*std::string module_indextoname(int i){


	int stave_id = (i/8) +1;
	std::string stave_side;
	if( (i/4)%2 == 0) stave_side = "_A_";
	else stave_side = "_C_";
	
	int module_sector = (i%4) + 1;

	std::stringstream module_name;
	module_name.str(std::string());
	if(stave_id < 10) module_name << "LI_S0" << stave_id << stave_side << "_M" << module_sector << "_";
	else module_name << "LI_S" << stave_id << stave_side << "_M" << module_sector << "_";

	return module_name.str();

}

void module_data_constructor(module_data MD, std::string module_name){

	std::string temp_distro_name = module_name + "_temp_distro";
	std::string temp_history_name = module_name + "_temp_history";
	std::string power_distro_name = module_name + "_power_distro";
	std::string power_history_name = module_name + "_power_history";
	std::string hv_v_distro_name = module_name + "_hv_v_distro";
	std::string hv_v_history_name = module_name + "_hv_v_history";
	std::string hv_i_distro_name = module_name + "hv_i_distro";
	std::string hv_i_history_name = module_name + "hv_i_history";

	MD.temp_distro = new TH1D(temp_distro_name.c_str(), temp_distro_name.c_str(), 301, -30.5, 30.5);
	MD.power_distro = new TH1D(power_distro_name.c_str(), power_distro_name.c_str(), 301, -0.5, 10.5);
	MD.hv_v_distro = new TH1D(hv_v_distro_name.c_str(), hv_v_distro_name.c_str(), 301, -300.5, -0.5); 
	MD.hv_i_distro = new TH1D(hv_i_distro_name.c_str(), hv_i_distro_name.c_str(), 301, -3e-3, 0); 
	MD.temp_history = new TGraph();
	MD.power_history = new TGraph();
	MD.hv_v_history = new TGraph();
	MD.hv_i_history = new TGraph();

}
*/
/*void set_point_graph(std::string set_point, run_time_interval rti){

		ifstream setPoint_stream;
		setPoint_stream.open(set_point.c_str());
		if(setPoint_stream.is_open()){
			std::stringstream sP_buffer;
			sP_buffer << setPoint_stream.rdbuf();
			std::stringstream sP_selection;
			select_data(rti, sP_buffer, sP_selection);

			std::vector<double> vTime, vTemp;
			double time, temp;
			std::string descriptor;//,timestamp, date, hours;
			while(sP_selection){
				sP_selection >> descriptor;
				sP_selection >> time;
				sP_selection >> temp;		
		//		std::cout<<descriptor<<"\t"<<temp<<"\t"<<time<<std::endl;			
				vTime.push_back(time);
				vTemp.push_back(temp);	
			}
			TGraph* sP_graph = new TGraph(vTime.size(), &(vTime[0]), &(vTemp[0]));
			sP_graph->SetTitle(descriptor.c_str());
			sP_graph->Write("SetPoint_Evolution");
		}



}*/

