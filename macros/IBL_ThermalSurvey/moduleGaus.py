from ROOT import gROOT, TCanvas, TH1F, TF1
import numpy as np
#import matplotlib.pyplot as plt

MAX_HOUR = 8

gROOT.Reset()
c1 = TCanvas('c1','c1',1400,400)
hist = TH1F('hist','hist', 50, -16.5, -14.5);
file = open('M9_ext_mod_temperature.txt','r')
line = file.readline()
temp_list = []
hist_list = []

for i in range(1,15):
		for k in ('_A_','_C_'):
			for j in range(1,5):
				hist = TH1F('stave_'+str(i)+k+'_mod_'+str(j),'stave_'+str(i)+k+'_mod_'+str(j), 500, -20, -10)
				hist_list.append(hist)
while line:
	values = line.split('  ')
	#print line
	if(len(values) == 3):
		NTC_name = values[0]
		NTC_temperature = float(values[1])
		NTC_date = values[2]
		NtcHour =( NTC_date.split(' ') )[1].split(':')
		for i in range(1,15):
			if i<10:
				stave_str = 'LI_S0' + str(i)
			else: 
				stave_str = 'LI_S' +str(i)
			for k in ('_A_','_C_'):
				for j in range(1,5):
					mod_str = stave_str+k+'M'+str(j)
					if(int(NtcHour[0])<MAX_HOUR and mod_str in NTC_name):
						print NTC_name + ' ' + NtcHour[0] + ' ' + str(NTC_temperature) 
						temp_list.append([NTC_temperature,i,k,j])
						if k == '_A_':
							hist_list[(i-1)*8 +j-1 ].Fill(NTC_temperature)		
						else:
							hist_list[(i-1)*8+4 +j-1 ].Fill(NTC_temperature)		
							

	line = file.readline()
file.close()
#c1.Divide(7,2)
#hist.SetFillColor('kRed');

overall_histo = TH1F('overall_histo', 'overall_histo', 100, -0.5, 0.5)
#rms_list = []
for i in range(1,113):
#	c1.cd(i)
	if i==1:
		hist_list[i-1].Draw()
	else:
		hist_list[i-1].Draw('same')
		
	#hist_list[i-1].Fit('gaus')
	
#	print gaus.GetParameter(2)
for temp in temp_list:
	t = temp[0]
	s = temp[1]
	if temp[2] == '_A_':
		side = 0
	else:
		side = 4
	m = temp[3]

	overall_histo.Fill(t - hist_list[(s-1)*8+side+m-1].GetMean())
c2 = TCanvas('c2','c2',800,600)
overall_histo.Draw();
overall_histo.Fit('gaus')
overall_histo.GetFunction('gaus').SetNpx(10000)
print 'SIGMA IS: '+str(overall_histo.GetFunction('gaus').GetParameter(2))
c1.Print('canvas.eps')
c1.WaitPrimitive()
