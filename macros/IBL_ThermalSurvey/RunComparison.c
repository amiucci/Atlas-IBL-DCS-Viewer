#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include "TGraph.h"
#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include <string>
#include <sstream>
#include <fstream>
#include "TMultiGraph.h"
#include "TPad.h"
#include "TLegend.h"
#include "THStack.h"
#include "TH2D.h"
#include "TStyle.h"
#include "TColor.h"
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TPad.h"
#include "TLatex.h"
#include "TDirectory.h"

struct run_time_interval{
	std::string run_number;
	double start_time;
	double stop_time;
	double ready_start;
	double ready_stop;
	double mean_temperature;
	double mean_rms;
	double mean_temperature_cool;
	double mean_rms_cool;
	bool ready;
};

struct module_data{
	std::string module;
	double ready_start;
	double ready_stop;
	TH1D* temp_distro; 
	double temp_max;
	double temp_min;
	double excursion;
};

struct cooling_data{
	std::string module;
	TH1D* temp_distro; 
	double temp_max;
	double temp_min;
	double excursion;
};

module_data mod_master(std::string module_name, TFile* file);
cooling_data cooling_master(std::string module_name, TFile *file, module_data MD);

void runComparison(){

	TFile *run_1 = new TFile("/afs/cern.ch/user/a/amiucci/work/public/IBL_temp_monitoring/W1_runQuery/267167.root");
	TFile *run_2 = new TFile("/afs/cern.ch/user/a/amiucci/work/public/IBL_temp_monitoring/W2_runQuery/270441.root");
	TH1D *temp_diff = new TH1D("temp_diff","temp_diff", 40, -1, 1);

	std::stringstream module_id, long_id;
	for(int stave_id = 1; stave_id <=14; stave_id++){
		for(int side = 0; side <=1; side++){
			std::string side_str;
			if(side == 0 ) side_str = "_A_M";
			else side_str = "_C_M";
			for(int sector = 1; sector <=4 ; sector++){
				module_id.str(std::string());
				long_id.str(std::string());
				if(stave_id<10) module_id<<"LI_S0"<<stave_id<<side_str<<sector<<"_";
				else module_id<<"LI_S"<<stave_id<<side_str<<sector<<"_";

				long_id<<"ModulesTemperatures/"<<module_id.str()<<"/";
				std::string long_str = long_id.str();
				std::string module_name = long_str+module_id.str()+"TModule";	
				//run_1->cd(long_str.c_str());
				TGraph *g1 = (TGraph*)run_1->Get(module_name.c_str());
				TGraph *g2 = (TGraph*)run_2->Get(module_name.c_str());
				g1->Draw();	
				std::vector<double> g1_vect (g1->GetY(),g1->GetY() + sizeof(g1->GetY())/sizeof(g1->GetY()[0]) );				
				std::vector<double> g2_vect (g2->GetY(),g2->GetY() + sizeof(g2->GetY())/sizeof(g2->GetY()[0]) );				
				double mx_g1=-1000, mx_g2=-10000;
				for(int i=0; i<g1_vect.size(); i++) if(mx_g1 < g1_vect[i]) mx_g1 = g1_vect[i];
				for(int i=0; i<g2_vect.size(); i++) if(mx_g2 < g2_vect[i]) mx_g2 = g2_vect[i];
				std::cout<< mx_g2 << "\t" << mx_g1 << std::endl;
				temp_diff->Fill(mx_g2 - mx_g1);	

			}			

		}

	}
	TCanvas *c = new TCanvas("c","c",800,600);	
	temp_diff->Draw();
}


/*void SmartPlotRuns(std::string week){

	std::string runlist_file = week + "_runQuery.txt";
	std::string temp_file = week + "_mod_temperature.txt";
	std::string folder = "/afs/cern.ch/user/a/amiucci/work/public/IBL_temp_monitoring/"+runlist_file.substr(0,runlist_file.size()-4)+"/";


	std::vector<run_time_interval> RTIs;

	std::string run;
	double start_t, stop_t;
	ifstream runListFile;
	runListFile.open(runlist_file.c_str());
	std::stringstream buffer;
	buffer << runListFile.rdbuf();

	while(buffer){
		
		buffer>>run;
		buffer>>start_t;
		buffer>>stop_t;
		run_time_interval RTI;
		RTI.run_number = run;
		RTI.start_time = start_t;
		RTI.stop_time = stop_t;
		RTIs.push_back(RTI);
		std::cout<<run<<std::endl;
	}
	runListFile.close();

	TFile* file;


	std::vector<double> v_times, v_time_widths, v_mean_rms, v_err_rms, v_mean_exc, v_err_exc;
	std::vector<double> v_ctimes, v_ctime_widths, v_cmean_rms, v_cerr_rms, v_cmean_exc, v_cerr_exc;

	for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
		(*it).ready = false;
		std::vector<module_data> mdv;
		std::vector<cooling_data> cdv;
		std::string filename = folder + (*it).run_number + ".root";
		file = new TFile(filename.c_str());

		char NTC_char[] = "abcdefghijklmn";

		for(int stave_id = 1; stave_id <=14; stave_id++){
			std::stringstream st_id;
			st_id.str(std::string());
			if(stave_id < 10) st_id<<"LI_S0"<<stave_id;
			else st_id<<"LI_S"<<stave_id;


		//	std::cout<<"STAVE ID:"<<st_id.str()<<std::endl;
			
			for(int i = 0; i<=1; i++){
				std::stringstream st_side;
				st_side.str(std::string());
				if(i == 0 ) st_side<<"_A_";
				else st_side<< "_C_";

				for(int sector = 1; sector<=4; sector++){
					std::stringstream sector_id;
					sector_id.str(std::string());
					sector_id << st_id.str() << st_side.str() << "M" << sector << "_";
					//std::cout << sector_id.str() <<std::endl;
				
					mdv.push_back(mod_master(sector_id.str(), file));

				}

			}
			std::stringstream st_cool;
			st_cool.str(std::string());
			if(stave_id % 2 == 1) st_cool<<st_id.str()<<"_C_ENV_TT"<< NTC_char[stave_id-1] <<"30";
			else st_cool<<st_id.str()<<"_A_ENV_TT"<< NTC_char[stave_id-1] <<"30"; 
			//st_cool << st_id << "*ENV_TT*30";
			cdv.push_back(cooling_master(st_cool.str(), file, mdv.at(0)));	
		}


		TH1D* rms_distro = new TH1D("rms_distro","rms_distro", 200, 0, 50);
		TH1D* rms_cool_distro = new TH1D("rms_cool_distro","rms_cool_distro", 200, 0, 50);
		double max_excursion=-1000, min_excursion=1000;

		std::cout<<"RUN NUMBER: "<<(*it).run_number<<std::endl;
		for(std::vector<module_data>::iterator it = mdv.begin(); it!=mdv.end(); ++it){
			//std::cout<<(*it).excursion<<std::endl;
			rms_distro->Fill((*it).temp_distro->GetRMS());
			if((*it).excursion >= max_excursion && (*it).excursion > 0) max_excursion = (*it).excursion;
			if((*it).excursion <= min_excursion && (*it).excursion > 0) min_excursion = (*it).excursion;
		}
		if(max_excursion > -1000){
			(*it).ready = true;
			(*it).ready_start = mdv.at(0).ready_start;
			(*it).ready_stop = mdv.at(0).ready_stop;
			v_times.push_back((mdv.at(0).ready_stop + mdv.at(0).ready_start)/2);
			v_time_widths.push_back((mdv.at(0).ready_stop - mdv.at(0).ready_start)/2);
			v_mean_rms.push_back(rms_distro->GetMean());
			v_err_rms.push_back(rms_distro->GetRMS());
			v_mean_exc.push_back((max_excursion + min_excursion)/2);
			v_err_exc.push_back((max_excursion - min_excursion)/2);	
	
		}

		std::cout<<"MAX_EX: "<<max_excursion<<std::endl;
		std::cout<<"MIN_EX: "<<min_excursion<<std::endl;

		double max_cool_excursion=-1000, min_cool_excursion=1000;
		for(std::vector<cooling_data>::iterator it = cdv.begin(); it != cdv.end(); ++it){
			rms_cool_distro->Fill((*it).temp_distro->GetRMS());
			if((*it).excursion >= max_cool_excursion) max_cool_excursion = (*it).excursion;
			if((*it).excursion <= min_cool_excursion) min_cool_excursion = (*it).excursion;
		}

		std::cout<<"MAX_COOL_EX: "<<max_cool_excursion<<std::endl;
		std::cout<<"MIN_COOL_EX: "<<min_cool_excursion<<std::endl;

		if(max_cool_excursion > -1000){
			v_cmean_rms.push_back(rms_cool_distro->GetMean());
			v_cerr_rms.push_back(rms_cool_distro->GetRMS());
			v_cmean_exc.push_back((max_cool_excursion + min_cool_excursion)/2);
			v_cerr_exc.push_back((max_cool_excursion - min_cool_excursion)/2);	
		}
	}
	TGraphErrors *rTemp = new TGraphErrors(v_times.size()-1, &(v_times[0]), &(v_mean_rms[0]) ,&(v_time_widths[0]), &(v_err_rms[0]) );
	TGraphErrors *pTemp = new TGraphErrors(v_times.size()-1, &(v_times[0]), &(v_mean_exc[0]) ,&(v_time_widths[0]), &(v_err_exc[0]) );
	TGraphErrors *rcTemp = new TGraphErrors(v_times.size()-1, &(v_times[0]), &(v_cmean_rms[0]) ,&(v_time_widths[0]), &(v_cerr_rms[0]) );
	TGraphErrors *pcTemp = new TGraphErrors(v_times.size()-1, &(v_times[0]), &(v_cmean_exc[0]) ,&(v_time_widths[0]), &(v_cerr_exc[0]) );

//	gStyle->SetOptTitle(0);	
	rTemp->SetFillColor(kOrange-2);
	//rTemp->SetFillStyle(3004);
	pTemp->SetFillColor(kRed);
	pTemp->SetFillStyle(3004);
	
	TCanvas* c1 = new TCanvas("c1","c1", 1200, 600);
	rTemp->Draw("AI2");
	rTemp->GetXaxis()->SetTimeDisplay(1);
	rTemp->GetXaxis()->SetTimeFormat("%b-%d %Hh %F1970-01-01 00:00:00");
	rTemp->Draw("AI2");
	pTemp->Draw("I2same");
	rTemp->GetYaxis()->SetRangeUser(0,0.7);
	rTemp->GetXaxis()->SetTitle("Date");
	rTemp->GetYaxis()->SetTitle("Temperature [K]");
	gPad->Update();
	TLegend *leg_1 = new TLegend(0.1,0.65,0.5,0.85);
	leg_1->SetFillStyle(0);
	leg_1->SetBorderSize(0);
	leg_1->AddEntry(rTemp,"Module Temperature RMS distribution","F");
	leg_1->AddEntry(pTemp,"Module TemperaturePeak to Peak excursion","F");
	leg_1->Draw();
	TLatex *lx1 = new TLatex();
	lx1->SetNDC(kFALSE);
	lx1->SetTextSize(0.04);
	lx1->SetTextAlign(12);
	lx1->SetTextAngle(90);
	for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
		double time = ((*it).ready_stop + (*it).ready_start)/2;
		//std::cout<<"time "<<time<<std::endl;
	
		if((*it).ready)lx1->DrawText(time,(*it).mean_rms+0.03,((*it).run_number).c_str());
	}
	lx1->Draw();


	TCanvas* c2 = new TCanvas("c2","c2", 1200, 600);
	rcTemp->SetFillColor(kAzure+7);
	//rTemp->SetFillStyle(3004);
	pcTemp->SetFillColor(kBlue+3);
	pcTemp->SetFillStyle(3004);
	rcTemp->Draw("AI2");
	rcTemp->GetXaxis()->SetTimeDisplay(1);
	rcTemp->GetXaxis()->SetTimeFormat("%b-%d %Hh %F1970-01-01 00:00:00");
	rcTemp->Draw("AI2");
	pcTemp->Draw("I2same");
	rcTemp->GetYaxis()->SetRangeUser(0,0.3);
	rcTemp->GetXaxis()->SetTitle("Date");
	rcTemp->GetYaxis()->SetTitle("Temperature [K]");
	gPad->Update();
	TLegend *leg_2 = new TLegend(0.1,0.65,0.5,0.85);
	leg_2->SetFillStyle(0);
	leg_2->SetBorderSize(0);
	leg_2->AddEntry(rcTemp,"Cooling Temperature RMS distribution","F");
	leg_2->AddEntry(pcTemp,"Cooling TemperaturePeak to Peak excursion","F");
	leg_2->Draw();
	TLatex *lx2 = new TLatex();
	lx2->SetNDC(kFALSE);
	lx2->SetTextSize(0.04);
	lx2->SetTextAlign(12);
	lx2->SetTextAngle(90);
	int counter = 0;
	for(std::vector<run_time_interval>::iterator it = RTIs.begin(); it!=RTIs.end(); ++it){
		
		//double time = v_times[counter];
		double time = ((*it).ready_stop + (*it).ready_start)/2;
		//std::cout<<"time "<<time<<std::endl;
	
		if((*it).ready) lx1->DrawText(time,(*it).mean_rms+0.03,((*it).run_number).c_str());
	}
	lx2->Draw();


//	c1->Print("DeltaModule.eps");
//	c2->Print("DeltaiCooling.eps");

}

*/

module_data mod_master(std::string module_name, TFile *file){
	module_data MD;

	MD.module = module_name;
	MD.temp_distro = new TH1D("temp_distro","temp_distro",200,-30,30 );	

	std::string folder = "ModulesTemperatures/" + module_name;	

	file->cd(folder.c_str());
	//gDirectory->pwd();	
	TGraph *power_graph, *temp_graph;
	std::string pn = module_name + "Power";
	std::string tn = module_name + "TModule";
	//gDirectory->ls();
	power_graph = (TGraph*) (gDirectory->Get( pn.c_str() ));
	temp_graph = (TGraph*) (gDirectory->Get( tn.c_str() ));
	//power_graph->SetDirectory(0);

//	std::cout<<temp_graph<<std::endl;
	//power_graph->Draw("AL");

	double* power_array =  power_graph->GetY();
	double* time_array =  power_graph->GetX();

	double* temp_array =  temp_graph->GetY();
	double* ttime_array =  temp_graph->GetX();

	for(int i=0 ; i<power_graph->GetN(); i++){
		//std::cout<<power_graph->GetN()<<std::endl;
		if(i!=0 && power_array[i] >= power_array[i-1] + 0.2) MD.ready_start = time_array[i];
		if(i!=0 && power_array[i] <= power_array[i-1] - 0.2) MD.ready_stop = time_array[i-1];
	}


	MD.temp_max = -1000;
	MD.temp_min = 1000;
	for(int i=0; i < temp_graph->GetN(); i++){
		if(ttime_array[i] >= MD.ready_start && ttime_array[i] <= MD.ready_stop){
			MD.temp_distro->Fill(temp_array[i]); 
			if(temp_array[i] >= MD.temp_max) MD.temp_max = temp_array[i];	
			if(temp_array[i] <= MD.temp_min) MD.temp_min = temp_array[i];	
		}
	}
	MD.excursion = MD.temp_max -MD.temp_min;
	return MD;
}

cooling_data cooling_master(std::string module_name, TFile *file, module_data MD){
	cooling_data CD;

	CD.module = module_name;
	CD.temp_distro = new TH1D("temp_distro","temp_distro",200,-30,30 );	

	std::string folder = "CoolingTemperatures/";	

	file->cd(folder.c_str());
	//gDirectory->pwd();	
	TGraph  *temp_graph;
	
	std::string tn = module_name;
	//gDirectory->ls();
	//std::cout << module_name <<std::endl;
	temp_graph = (TGraph*) (gDirectory->Get( tn.c_str() ));
	//power_graph->SetDirectory(0);

//	std::cout<<temp_graph<<std::endl;
	//power_graph->Draw("AL");


	double* temp_array =  temp_graph->GetY();
	double* ttime_array =  temp_graph->GetX();



	CD.temp_max = -1000;
	CD.temp_min = 1000;
	for(int i=0; i < temp_graph->GetN(); i++){
		if(ttime_array[i] >= MD.ready_start && ttime_array[i] <= MD.ready_stop){
			CD.temp_distro->Fill(temp_array[i]); 
			if(temp_array[i] >= CD.temp_max) CD.temp_max = temp_array[i];	
			if(temp_array[i] <= CD.temp_min) CD.temp_min = temp_array[i];	
		}
	}
	CD.excursion = CD.temp_max -CD.temp_min;
	return CD;
}

